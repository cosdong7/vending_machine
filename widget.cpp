#include "widget.h"
#include "ui_widget.h"
#include <QMessageBox>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    checkMoney();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::changeMoney(int n){
    money += n;
    ui->lcdNumber->display(QString::number(money));
    checkMoney();

}


void Widget::checkMoney(){
    if (money < 200){
        ui->pbCoke->setDisabled(true);
        if (money < 150){
            ui->pbTea->setDisabled(true);
            if (money < 100)    ui->pbCoffee->setDisabled(true);
            else    ui->pbCoffee->setDisabled(false);
        }
        else{
            ui->pbTea->setDisabled(false);
            ui->pbCoffee->setDisabled(false);
        }
    }
    else{
        ui->pbCoke->setDisabled(false);
        ui->pbTea->setDisabled(false);
        ui->pbCoffee->setDisabled(false);
    }
}

void Widget::on_pb10_clicked()
{
    changeMoney(10);
}

void Widget::on_pb50_clicked()
{
    changeMoney(50);
}

void Widget::on_pb100_clicked()
{
    changeMoney(100);
}

void Widget::on_pb500_clicked()
{
    changeMoney(500);
}

void Widget::on_pbCoffee_clicked()
{
    changeMoney(-100);
}

void Widget::on_pbTea_clicked()
{
    changeMoney(-150);
}

void Widget::on_pbCoke_clicked()
{
    changeMoney(-200);
}

void Widget::on_pbRefund_clicked()
{
    int c10, c50, c100, c500;
    c500 = money / 500;
    money %= 500;
    c100 = money / 100;
    money %= 100;
    c50 = money / 50;
    money %= 50;
    c10 = money / 10;
    changeMoney(-money);
    QMessageBox msg;
    msg.information(nullptr, "refund", QString::number(c500) + "*500 + " + QString::number(c100) + "*100 + " + QString::number(c50) + "*50 + " + QString::number(c10) + "*10 coins refunded");
}
